from django.contrib import admin
from weather.models import Country, City, WeatherData


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    pass


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass


@admin.register(WeatherData)
class WeatherDataAdmin(admin.ModelAdmin):
    pass
