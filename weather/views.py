from rest_framework import viewsets, serializers
from weather.models import WeatherData
from weather.serializers import WeatherDataSerializer


class WeatherViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Get latest weather data.

    list:
    Get list of weather data.

    create:
    Create weather data.

    delete:
    Remove weather data.
    """
    queryset = WeatherData.objects.all()
    serializer_class = WeatherDataSerializer
    filter_fields = ('on_time', 'city')
