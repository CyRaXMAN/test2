
from rest_framework import routers
from weather.views import WeatherViewSet

router = routers.DefaultRouter()
router.register(r'weather', WeatherViewSet)

urlpatterns = router.urls