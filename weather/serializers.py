from rest_framework import serializers
from weather.models import Country, City, WeatherData


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        exclude = ('id',)


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('name', 'location')


class WeatherDataSerializer(serializers.ModelSerializer):
    city = CitySerializer(many=False)

    class Meta:
        model = WeatherData
        exclude = ('id',)
