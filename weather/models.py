from django.db import models
# from django.contrib.gis.db import models as gis_models
from django.utils import timezone


class Country(models.Model):

    name = models.CharField(verbose_name="Country name", max_length=200)

    def __str__(self):
        return self.name


class City(models.Model):

    name = models.CharField(verbose_name="City name", max_length=200)
    country = models.ForeignKey(Country, verbose_name="Country", on_delete=models.CASCADE)
    coordinates_latitude = models.DecimalField(verbose_name="Latitude", max_digits=9, decimal_places=6)
    coordinates_longitude = models.DecimalField(verbose_name="Longitude", max_digits=9, decimal_places=6)

    @property
    def location(self):
        return {'latitude': self.coordinates_latitude, 'longitude': self.coordinates_longitude}

    def __str__(self):
        return self.name


class WeatherData(models.Model):

    on_time = models.DateTimeField(verbose_name="On time", default=timezone.now)
    city = models.ForeignKey(City, verbose_name="City", on_delete=models.CASCADE)
    temperature = models.DecimalField(verbose_name="Temperature", max_digits=7, decimal_places=3)
    wind_speed = models.DecimalField(verbose_name="Wind speed", max_digits=10, decimal_places=2)
    wind_degree = models.IntegerField(verbose_name="Wind degree")

    def __str__(self):
        return "For {} on {}".format(self.city.name, self.on_time)
